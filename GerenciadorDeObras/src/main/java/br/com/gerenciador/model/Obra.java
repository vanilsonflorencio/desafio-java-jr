package br.com.gerenciador.model;	

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;


@Entity
@Table(name="obra")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Obra {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long codigo;
	
	@Column
	@NotNull
	private String nome;

	@Column
	private String descricao;

	@Column	@DateTimeFormat(pattern = "yyyy-MM-dd")

	private Date dtpublicacao;
	
	@Column @DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dtexposicao;
	
	@Column
	private String img;


	@ManyToMany( fetch = FetchType.EAGER, mappedBy="obra")
	@JsonIgnoreProperties("obra")
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	private List<Autor> autor;

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}



	public Date getDtpublicacao() {
		return dtpublicacao;
	}

	public void setDtpublicacao(Date dtpublicacao) {
		this.dtpublicacao = dtpublicacao;
	}

	public Date getDtexposicao() {
		return dtexposicao;
	}

	public void setDtexposicao(Date dtexposicao) {
		this.dtexposicao = dtexposicao;
	}

	public void setAutor(List<Autor> autor) {
		this.autor = autor;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Collection<Autor> getAutor() {
		return autor;
	}

	public void setAutor(ArrayList<Autor> autor) {
		this.autor = autor;
	}

	public long getCodigo() {
		return codigo;
	}

	@Override
	public boolean equals(Object obj) {
		
		Obra other = (Obra) obj;
		if (autor == null) {
			if (other.autor != null)
				return false;
		} else if (!autor.equals(other.autor))
			return false;
		if (codigo != other.codigo)
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (dtexposicao == null) {
			if (other.dtexposicao != null)
				return false;
		} else if (!dtexposicao.equals(other.dtexposicao))
			return false;
		if (dtpublicacao == null) {
			if (other.dtpublicacao != null)
				return false;
		} else if (!dtpublicacao.equals(other.dtpublicacao))
			return false;
		if (img == null) {
			if (other.img != null)
				return false;
		} else if (!img.equals(other.img))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	
}