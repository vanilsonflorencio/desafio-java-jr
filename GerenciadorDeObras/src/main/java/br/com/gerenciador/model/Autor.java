package br.com.gerenciador.model;		

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Email;

import org.hibernate.annotations.Cascade;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;


@Entity
@Table(name="autor")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Autor {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long codigo;
	
	@Column
	@NotNull
	private String nome;

	@Column
	@NotNull
	private String sexo;

	@Column
	@DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dtnascimento;
	
	@Column
	private String pais;
	
	@Column
	@CPF
	@Valid
	private String cpf;

	@Column 
	@Email
	private String email;
	
	@ManyToMany()
	@JoinColumn(name="autor_obra")
	@JsonIgnoreProperties("autor")
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	private List<Obra> obra;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}	

	public Date getDtnascimento() {
		return dtnascimento;
	}

	public void setDtnascimento(Date dtnascimento) {
		this.dtnascimento = dtnascimento;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public long getCodigo() {
		return codigo;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}	

	public List<Obra> getObra() {
		return obra;
	}

	public void setObra(List<Obra> obra) {
		this.obra = obra;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Autor other = (Autor) obj;
		if (codigo != other.codigo)
			return false;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		if (dtnascimento == null) {
			if (other.dtnascimento != null)
				return false;
		} else if (!dtnascimento.equals(other.dtnascimento))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (obra == null) {
			if (other.obra != null)
				return false;
		} else if (!obra.equals(other.obra))
			return false;
		if (pais == null) {
			if (other.pais != null)
				return false;
		} else if (!pais.equals(other.pais))
			return false;
		if (sexo == null) {
			if (other.sexo != null)
				return false;
		} else if (!sexo.equals(other.sexo))
			return false;
		return true;
	}

	public ArrayList<Autor> toArrayList(Autor a) {
		ArrayList<Autor> list = new ArrayList<Autor>();
		list.add(a);
		return list;
	}
}