package br.com.gerenciador.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.gerenciador.model.Autor;
import br.com.gerenciador.repository.AutorRepository;


@Scope("application")
@Service
public class AutorService {

	@Autowired
	AutorRepository autorRepository;
	
	public AutorService() {
		
	}
	public Autor save(Autor autor) {
		return autorRepository.save(autor);
	}	
	
	public List<Autor> getAll() {
		return autorRepository.findAll();
	}
	
	public Autor getByCode(long cod) {
		return autorRepository.getOne(cod);
	}
	
	public void delete(Long cod) {
		delete(getByCode(cod));
	}
	
	public void delete(Autor autor) {
		if(autor != null && autor.getObra().size() == 0) {	
			
			try {
				System.out.println("******************" + autor.getObra().size());
				autorRepository.deleteById(autor.getCodigo());
			} catch (Exception e) {
			}
		}
	}
	
	public void deleteAll() {
		
		autorRepository.deleteAll();
	}
	
	//TODO
	public Autor adicionarObra(long codautor, long codobra) {
		Autor a = getByCode(codautor);
		
		return save(a);		
	}
	
	public Autor update(long cod, Autor autor) {
		Autor original = getByCode(cod);	
		
		BeanUtils.copyProperties(autor, original, "codigo");
		return save(original);		
	}
	
	public boolean verify() {
		
		return true;
	}

	public Autor getAutorByCpf(String cpf) {
		try {
			Autor temp = autorRepository.findByCpf(cpf);
			return temp;
			
		} catch (Exception e) {
			return null	;
		}
	}
}