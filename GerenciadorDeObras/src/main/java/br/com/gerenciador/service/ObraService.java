package br.com.gerenciador.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import br.com.gerenciador.model.Autor;
import br.com.gerenciador.model.Obra;
import br.com.gerenciador.repository.ObraRepository;


@Scope("application")
@Service
public class ObraService {

	@Autowired
	ObraRepository obraRepository;
	
	@Autowired
	AutorService autorservice;
	
	public Obra save(Obra obra) throws Exception {
		validate(obra);
		return obraRepository.save(obra);
	}	
	
	public List<Obra> getAll() {
		return obraRepository.findAll();
	}
	
	public List<Obra> getByName(String titulo) {
		return obraRepository.buscarPorTitulo(titulo.toLowerCase());
	}
	
	public List<Obra> getByDescription(String descricao) {
		return obraRepository.buscarPorDescricao(descricao.toLowerCase());
	}
	
	public Obra getByCode(long cod) {
		return obraRepository.getOne(cod);
	}
	public void delete(Long cod) {
		obraRepository.deleteById(cod);
	}
	
	public void delete(Obra obra) {
		obraRepository.delete(obra);
	}
	
	public void deleteAll() {
		obraRepository.deleteAll();
	}
	
	public Obra update(long cod, Obra obra) throws Exception {
		Obra original = getByCode(cod);	
		
		BeanUtils.copyProperties(obra, original, "codigo");
		return save(original);		
	}
	
	public void addAutor(long obraCod, long autorCod) {
		try {
			Autor autor = autorservice.getByCode(autorCod);
			
			Obra original = getByCode(autorCod);	
			original.getAutor().add(autor);
			autor.getObra().add(original);
			
			save(original);
			autorservice.save(autor);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
	
	public void validate(Obra obra) throws Exception {		
		if(obra == null)
			throw new Exception("Obra is null.");
//		if(obra.getAutor() == null || obra.getAutor().size() == 0)
//			throw new Exception("Obra need at least one author.");
		if(obra.getDtexposicao() == null && obra.getDtpublicacao() == null)
			throw new Exception("Obra hasn't a valid date.");

	}
}