package br.com.gerenciador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GerenciadorDeObrasApplication {

	public static void main(String[] args) {
		SpringApplication.run(GerenciadorDeObrasApplication.class, args);
	}

}
