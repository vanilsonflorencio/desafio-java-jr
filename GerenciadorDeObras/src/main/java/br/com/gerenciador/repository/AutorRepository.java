package br.com.gerenciador.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.gerenciador.model.Autor;

public interface AutorRepository  extends JpaRepository<Autor, Long>{
	
//	@Query("SELECT u FROM User u WHERE login = :login")
//	User findByLogin(@Param("login") String login);
	  
	@Query("SELECT a FROM Autor a WHERE a.cpf like :cpfAutor")
	Autor findByCpf(String cpfAutor);
}
