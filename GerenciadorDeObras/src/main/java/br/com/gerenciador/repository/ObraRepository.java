package br.com.gerenciador.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.gerenciador.model.Obra;

public interface ObraRepository  extends JpaRepository<Obra, Long>{

	@Query("select o from Obra o where lower(o.nome) like %:titulo%")
	List<Obra> buscarPorTitulo(@Param("titulo") String titulo);

	@Query("select a from Obra a where LOWER(a.descricao) like %:descricao%")
	List<Obra> buscarPorDescricao(@Param("descricao") String descricao);
}
