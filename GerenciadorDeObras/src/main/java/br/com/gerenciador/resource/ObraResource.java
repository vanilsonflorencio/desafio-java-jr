package br.com.gerenciador.resource;


import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.gerenciador.model.Obra;
import br.com.gerenciador.service.ObraService;



@RestController
@RequestMapping("/obra")
@CrossOrigin(maxAge=10, origins="*")
//@MultipartConfig(maxFileSize = 1024*5, maxRequestSize = 1024*5)

public class ObraResource {	
	
	@Autowired
	ObraService obraService;
	
	/**
	 * Used to get all registers of the Obra tables.
	 * @return List Obra
	 */
	@GetMapping ("/all")
	public  List<Obra> listar(){
		return obraService.getAll();
	}
	
	/**
	 * <strong>Method used to add a new Obra entity on system.</strong>
	 * 
	 * @param Obra entity
	 * @param HttpServletResponse response
	 * @return ResponseEntity
	 */
	@PostMapping
	public ResponseEntity<Object> criar(Obra obra, HttpServletResponse response) {
		Obra savedObra;
		try {
			savedObra = obraService.save(obra);		
			return ResponseEntity.status(HttpStatus.CREATED).body(savedObra);
			
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());

		}
	}
	
	
	

	@GetMapping("/cod/{codigo}")
	public ResponseEntity<Obra> buscarPeloCodigo(@PathVariable Long codigo) {
		Obra obra = obraService.getByCode(codigo);
		
		if (obra != null)
			return ResponseEntity.ok(obra);
		else
			return ResponseEntity.notFound().build();
	}
	@GetMapping("/titulo")
	public ResponseEntity <List<Obra>> buscarPeloTitulo(@RequestBody Obra obra) {
		List <Obra> temp = obraService.getByName(obra.getNome());

		if (temp != null)
			return ResponseEntity.ok(temp);
		else
			return ResponseEntity.notFound().build();
	}
	
	@GetMapping("/descricao/{descricao}")
	public ResponseEntity <List<Obra>> buscarPelaDescricao(@PathVariable String descricao) {
		List <Obra> obra = obraService.getByDescription(descricao);
		
		if (obra != null)
			return ResponseEntity.ok(obra);
		else
			return ResponseEntity.notFound().build();
	}
	
	/**
	 * Used to developers to know all atributes used on this class.
	 * @return Obra A single instance with all values default.
	 */
	@GetMapping("/help")
	public Obra help() {
		Obra obra = new Obra(); 		
		return obra;
	}
	
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long codigo) {
		obraService.delete(codigo);
	}

	@PutMapping("/{codigo}")
	public  ResponseEntity<Object> atualizarUsuario(@PathVariable Long codigo,  @RequestBody Obra obra) {
		Obra savedObra;
		try {
			savedObra = (Obra) obraService.update(codigo, obra);	
			return ResponseEntity.status(HttpStatus.CREATED).body(savedObra);
			
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());

		}
		
	}
	
	@PutMapping("/{codigoobra}/{codigoautor}")
	public  ResponseEntity<Object> addAutor(@PathVariable Long codigoobra,  @PathVariable  Long codigoautor) {
		try {
			
			obraService.addAutor(codigoobra, codigoautor);	
			
			return ResponseEntity.status(HttpStatus.CREATED).body("Adicionado com sucesso!");
			
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());

		}
		
	}
}
