package br.com.gerenciador.resource;


import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.gerenciador.model.Autor;
import br.com.gerenciador.service.AutorService;



@RestController
@RequestMapping("/autor")
@CrossOrigin(maxAge=10, origins="*")
//@MultipartConfig(maxFileSize = 1024*5, maxRequestSize = 1024*5)

public class AutorResource {	
	
	@Autowired
	AutorService autorService;
	
	/**
	 * Used to get all registers of the Autor tables.
	 * @return List Autor
	 */
	@GetMapping ("/all")
	public  List<Autor> listar(){
		return autorService.getAll();
	}
	
	/**
	 * <strong>Method used to add a new Autor entity on system.</strong>
	 * 
	 * @param Autor entity
	 * @param HttpServletResponse response
	 * @return ResponseEntity
	 */
//	@PostMapping
//	public ResponseEntity<Autor> criar( @RequestBody Autor autor, HttpServletResponse response) {
	@RequestMapping( method = RequestMethod.POST)
	public ResponseEntity<Autor> criar(  Autor autor, HttpServletResponse response) {

		Autor autorSalvo = autorService.save(autor);
		
		
		return ResponseEntity.status(HttpStatus.CREATED).body(autorSalvo);
	}	

	@GetMapping("/{codigo}")
	public ResponseEntity<Autor> buscarPeloCodigo(@PathVariable Long codigo, HttpServletResponse response) {
		Autor autor = autorService.getByCode(codigo);
		
		if (autor != null)
			return ResponseEntity.ok(autor);
		else
			return ResponseEntity.notFound().build();
	}
//	@GetMapping("{codautor}/adicionarobra/{codobra}")
//	public ResponseEntity<Autor> adicionarObra(@PathVariable Long codautor, Long codobra) {
//		Autor autor = autorService.getByCode(codigo);
//		
//		if (autor != null)
//			return ResponseEntity.ok(autor);
//		else
//			return ResponseEntity.notFound().build();
//	}
	
	/**
	 * Used to developers to know all atributes used on this class.
	 * @return Autor A single instance with all values default.
	 */
	@GetMapping("/help")
	public Autor help() {
		Autor autor = new Autor(); 
		
		return autor;
	}
	
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long codigo) {
		autorService.delete(codigo);
	}

	@PutMapping("/{codigo}")
	public  ResponseEntity<Autor> atualizarUsuario(@PathVariable Long codigo,  @RequestBody Autor autor) {
		Autor newautor = (Autor) autorService.update(codigo, autor);
		return ResponseEntity.ok(newautor);
	}
}
