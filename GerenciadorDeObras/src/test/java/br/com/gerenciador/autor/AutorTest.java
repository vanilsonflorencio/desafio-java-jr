package br.com.gerenciador.autor;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.gerenciador.model.Autor;
import br.com.gerenciador.model.Obra;
import br.com.gerenciador.service.AutorService;
import br.com.gerenciador.service.ObraService;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AutorTest {
	
	@Autowired
	AutorService autorService ;
	
	@Autowired
	ObraService obraService ;
	
	String name1 = "Jose Espinola";
	String name2 = "Bruce Wayne";	
	
	private Autor createAutor1() {
		Autor a = new Autor();
		a.setNome(name1);
		a.setDtnascimento(new Date());
		a.setSexo("masculino");
		a.setCpf("677.938.320-19");
		
		return a;
	}
	
	private Autor createAutor2() {
		Autor a = new Autor();
		a.setNome(name2);
		a.setDtnascimento(new Date());
		a.setSexo("masculino");
		a.setCpf("387.905.616-19");
		
		return a;
	}
	
	@Test
	//Clean data table and prepare to add news values
	public void teste01() {
		autorService.deleteAll();
		
		Object autores = autorService.getAll();
		assertEquals(new ArrayList<>(), autores);		
	}
		
	@Test
	//Create and read values
	public void teste02() {
		Autor a1 = createAutor1();		
		Autor a2 = createAutor2();
		
		autorService.save(a1);
		autorService.save(a2);
		
		assertEquals(autorService.getAutorByCpf(a1.getCpf()).getNome(), name1);
		assertEquals(autorService.getAutorByCpf(a2.getCpf()).getNome(), name2);
		
		autorService.deleteAll();
	}
	

	@Test
	//Deleting and seaching for the check if has deleted.
	public void teste03() {
		Autor a1 = createAutor1();		
		Autor a2 = createAutor2();
		
		autorService.save(a1);
		autorService.save(a2);
		
		autorService.delete(a1.getCodigo());
		autorService.delete(a2.getCodigo());
		
		assertEquals(null, autorService.getAutorByCpf(a1.getCpf()));
		assertEquals(null, autorService.getAutorByCpf(a2.getCpf()));
		
	}
	
	@Test
	public void teste04() {
		Autor a1 = createAutor1();		
		Autor a2 = createAutor2();
		
		Obra o = new Obra();
		ArrayList<Obra> obras = new ArrayList<Obra>();
		obras.add(o);
		
		a1.setObra(obras);
		
		Obra o2 = new Obra();
		obras = new ArrayList<Obra>();
		obras.add(o2);
		
		a2.setObra(obras);
		
		autorService.save(a1);
		autorService.save(a2);
		
		autorService.delete(a1.getCodigo());
		autorService.delete(a2.getCodigo());
		
		assertEquals(true, autorService.getAutorByCpf(a1.getCpf()) != null);
		assertEquals(true, autorService.getAutorByCpf(a2.getCpf()) != null);
	}
		
}
