package br.com.gerenciador.obra;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.gerenciador.model.Autor;
import br.com.gerenciador.model.Obra;
import br.com.gerenciador.service.AutorService;
import br.com.gerenciador.service.ObraService;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//@Component
//@ComponentScan("br.com.gerenciador.service")
public class ObraTest {
	@Autowired
	AutorService autorService;
	
	@Autowired
	ObraService obraService ;
	
	Obra o1; //Valid
	Obra o2; //Valid
	Obra o3; //Invalid
	
	Autor a1;
	
	//Setting initial values
	void config(){
		
		a1 = new Autor();
		a1.setNome("Antonio Nunes");
		a1.setDtnascimento(new Date());
		a1.setSexo("masculino");
		a1.setCpf("677.938.320-19");		
				
		o1 = new Obra();
		o1.setNome( "Memorias postumas");
		o1.setDtpublicacao(new Date());
		
		o2 = new Obra();
		o2.setNome("Guinnes Book");	
		o2.setDtpublicacao(new Date());
		ArrayList<Autor> autores = new ArrayList();
		autores.add(a1);
		o2.setAutor(autores);

		o3 = new Obra();
		o3.setNome("Romeu e Julieta");
	}
	
	@Test
	//Clean DB and verify
	public void test1() {
		obraService.deleteAll();
		
		Object obras = obraService.getAll();
		assertEquals(new ArrayList<>(), obras);		
	}
		
	@Test
	//Save and get test
	public void test2() {	
		obraService.deleteAll();
		autorService.deleteAll();
		
		config();

		Obra saved1 = null;
		Obra saved2 = null;
	
		try {
			saved1 = obraService.save(o1);
			saved2 = obraService.save(o2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		assertEquals(true,saved1!= null && obraService.getByCode(saved1.getCodigo())!= null  );
		assertEquals(true,saved2!= null && obraService.getByCode(saved2.getCodigo())!= null  );
	}
	
	
	@Test
	//Save and search by name
	public void test3() {	
		
		Autor a = new Autor();
		a.setNome("Antonio Nunes");
		a.setDtnascimento(new Date());
		a.setSexo("masculino");
		a.setCpf("677.938.320-19");
		
		try {
			autorService.deleteAll();
			a = autorService.save(a);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		obraService.deleteAll();
		
		try {
			obraService.save(o1);
			obraService.save(o2);	
		} catch (Exception e) {
			
		}

		assertEquals(true,obraService.getByName("Memor") != null  );
		assertEquals(true,obraService.getByName("book" ) != null  );
	}
	
	@Test
	//Save and search with valid date.
	public void test4() {
		obraService.deleteAll();

	}
		
}
